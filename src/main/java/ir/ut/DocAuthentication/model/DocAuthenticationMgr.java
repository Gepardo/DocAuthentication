package ir.ut.DocAuthentication.model;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import ir.ut.DocAuthentication.AbstractFile;
import ir.ut.DocAuthentication.repository.AbstractFileRepository;
import ir.ut.utility.Importer;

@Service
@Configurable
public class DocAuthenticationMgr {

	private static AbstractFileRepository abstractFileRepository;

	private static DocAuthenticationMgr instance = null;

	public static DocAuthenticationMgr getInstance(AbstractFileRepository abstractFileRepository1) {
		if (instance == null) {
			abstractFileRepository = abstractFileRepository1;
			instance = new DocAuthenticationMgr();
		}
		return instance;
	}

	public static DocAuthenticationMgr getInstance() {
		if (instance == null) {
			instance = new DocAuthenticationMgr();
		}
		return instance;
	}

	public AbstractFile addDoc(byte[] docByteArray, Long docDate, String docNumber, String name, Long expiryDate,
			String key, String finalPath) {

		byte[] newDocByteArray = null;
		try {
			newDocByteArray = addTextWatermark("مرکز فناوری اطلاعات دانشگاه تهران", docByteArray);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (FontFormatException e) {
			e.printStackTrace();
		}
		String hashCode = writeToFileSystem(newDocByteArray, finalPath);
		AbstractFile abstractFile = new AbstractFile(name, docDate, docNumber, key, hashCode, expiryDate);
		return abstractFileRepository.save(abstractFile);
	}

	private String writeToFileSystem(byte[] docByteArray, String finalPath) {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos;
		try {
			oos = new ObjectOutputStream(baos);
			oos.writeObject(docByteArray);
			oos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		MessageDigest messageDigit = null;
		try {
			messageDigit = MessageDigest.getInstance("SHA1");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		messageDigit.update(baos.toByteArray());

		BigInteger bigInt = new BigInteger(1, messageDigit.digest());
		String newPath = String.valueOf(bigInt);
		for (int index = 0; index < 10; index++) {
			finalPath = finalPath + File.separator + newPath.substring(0, 2);
			newPath = newPath.substring(2);
		}
		finalPath = finalPath + File.separator + newPath;
		try {
			InputStream inputStream = new ByteArrayInputStream(docByteArray);
			ImageQualityReducer(inputStream, finalPath);
			// FileUtils.writeByteArrayToFile(new File(finalPath),
			// docByteArray);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return String.valueOf(bigInt);
	}

	private static byte[] addTextWatermark(String waterMark, byte[] docByteArray)
			throws IOException, FontFormatException {
		InputStream source = new ByteArrayInputStream(docByteArray);

		BufferedImage image = ImageIO.read(source);
		Graphics2D w = (Graphics2D) image.getGraphics();
		AlphaComposite alphaChannel = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.4f);
		w.setComposite(alphaChannel);
		w.setColor(Color.GRAY);
		w.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);	
// /home/user/workspace/DocAuthentication/src/main/java/ir/ut/utility/data
		String fName = "/home/user/workspace/DocAuthentication/src/main/java/ir/ut/utility/data/B_Nazanin.ttf";
		//String fName = Importer.class.getResource("../data/B_Nazanin.ttf").getPath();
		InputStream is = new FileInputStream(fName);
		Font nastaligh = Font.createFont(Font.TRUETYPE_FONT, is);
		w.setFont(nastaligh.deriveFont(Font.PLAIN, 160f));
		FontMetrics fontMetrics = w.getFontMetrics();
		Rectangle2D rect = fontMetrics.getStringBounds(waterMark, w);

		// calculate center of the image

		// add text overlay to the image
		w.translate(image.getWidth() / 2.0f, image.getHeight() / 2.0f);

		// Create a rotation transform to rotate the text based on the diagonal
		// angle of the picture aspect ratio
		AffineTransform at2 = new AffineTransform();
		double opad = image.getHeight() / (double) image.getWidth();
		double angle = Math.toDegrees(Math.atan(opad));
		double idegrees = -1 * angle;
		double theta = (2 * Math.PI * idegrees) / 360;
		at2.rotate(theta);
		w.transform(at2);

		// Now reposition our coordinates based on size (same as we would
		// normally
		// do to center on straight line but based on starting at center
		float x1 = (int) rect.getWidth() / 2.0f * -1;
		float y1 = (int) rect.getHeight() / 2.0f;
		w.translate(x1, y1);

		w.drawString(waterMark, 0.0f, 0.0f);

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(image, "jpg", baos);
		baos.flush();
		byte[] imageInByte = baos.toByteArray();
		baos.close();
		w.dispose();
		return imageInByte;

	}

	private static void ImageQualityReducer(InputStream inputStream, String destPath) throws IOException {
		Iterator<?> iter = ImageIO.getImageWritersByFormatName("jpg");
		ImageWriter writer = (ImageWriter) iter.next();
		ImageWriteParam iwp = writer.getDefaultWriteParam();
		iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
		iwp.setCompressionQuality(0.15f);
		File file = new File(destPath);
		if (!file.getParentFile().exists()) {
			file.getParentFile().mkdirs();
		}
		FileImageOutputStream output = new FileImageOutputStream(file);
		writer.setOutput(output);

		BufferedImage originalImage = ImageIO.read(inputStream);

		IIOImage image = new IIOImage(originalImage, null, null);
		writer.write(null, image, iwp);
		writer.dispose();
	}

	/*
	 * For convert Date
	 */
	public Long convertDateToGregorianCal(String docDate) {
		// 1397-03-12+1 T20:00:00.000Z
		String[] dateDetail = docDate.split("-");
		String year = dateDetail[0];
		String month = dateDetail[1];
		String day = dateDetail[2];
		int timeZoneIndex = day.indexOf("T");
		day = day.substring(0, timeZoneIndex);
		int dayNum = Integer.parseInt(day);
		dayNum += 1; // error of ui calendar !

		int julianDay = toJulianDay(Integer.parseInt(month), dayNum, Integer.parseInt(year));
		GregorianCalendar gregorianCalendar = julianDayToGregorianCalendar(julianDay);
		Date sddd = gregorianCalendar.getTime();
		sddd.getTime();
		sddd.toString();
		return gregorianCalendar.getTimeInMillis();
	}

	private GregorianCalendar julianDayToGregorianCalendar(int JulianDayNumber) {

		int j = 4 * JulianDayNumber + 139361631 + (4 * JulianDayNumber + 183187720) / 146097 * 3 / 4 * 4 - 3908;
		int i = (j % 1461) / 4 * 5 + 308;

		int gregorianDay = (i % 153) / 5 + 1;
		int gregorianMonth = ((i / 153) % 12) + 1;
		int gregorianYear = j / 1461 - 100100 + (8 - gregorianMonth) / 6;

		return new GregorianCalendar(gregorianYear, gregorianMonth - 1, gregorianDay);
	}

	private GregorianCalendar getGregorianFirstFarvardin(int jalaliYear) {
		int marchDay = 0;
		int[] breaks = { -61, 9, 38, 199, 426, 686, 756, 818, 1111, 1181, 1210, 1635, 2060, 2097, 2192, 2262, 2324,
				2394, 2456, 3178 };
		int gregorianYear = jalaliYear + 621;
		int jalaliLeap = -14;
		int jp = breaks[0];
		int jump = 0;
		for (int j = 1; j <= 19; j++) {
			int jm = breaks[j];
			jump = jm - jp;
			if (jalaliYear < jm) {
				int N = jalaliYear - jp;
				jalaliLeap = jalaliLeap + N / 33 * 8 + (N % 33 + 3) / 4;
				if ((jump % 33) == 4 && (jump - N) == 4)
					jalaliLeap = jalaliLeap + 1;
				int GregorianLeap = (gregorianYear / 4) - (gregorianYear / 100 + 1) * 3 / 4 - 150;
				marchDay = 20 + (jalaliLeap - GregorianLeap);
				if ((jump - N) < 6)
					N = N - jump + (jump + 4) / 33 * 33;
				break;
			}
			jalaliLeap = jalaliLeap + jump / 33 * 8 + (jump % 33) / 4;
			jp = jm;
		}
		return new GregorianCalendar(gregorianYear, 2, marchDay);
	}

	private int toJulianDay(int jalaliMonth, int jalaliDay, int jalaliYear) {

		GregorianCalendar gregorianFirstFarvardin = getGregorianFirstFarvardin(jalaliYear);
		int gregorianYear = gregorianFirstFarvardin.get(Calendar.YEAR);
		int gregorianMonth = gregorianFirstFarvardin.get(Calendar.MONTH) + 1;
		int gregorianDay = gregorianFirstFarvardin.get(Calendar.DAY_OF_MONTH);

		JulianCalendar julianFirstFarvardin = new JulianCalendar(gregorianYear, gregorianMonth, gregorianDay);

		int julianDay = julianToJulianDayNumber(julianFirstFarvardin) + (jalaliMonth - 1) * 31
				- jalaliMonth / 7 * (jalaliMonth - 7) + jalaliDay - 1;

		return julianDay;
	}

	private int julianToJulianDayNumber(JulianCalendar jc) {
		int julianYear = jc.getYear();
		int julianMonth = jc.getMonth();
		int JulianDay = jc.getDay();

		return (1461 * (julianYear + 4800 + (julianMonth - 14) / 12)) / 4
				+ (367 * (julianMonth - 2 - 12 * ((julianMonth - 14) / 12))) / 12
				- (3 * ((julianYear + 4900 + (julianMonth - 14) / 12) / 100)) / 4 + JulianDay - 32075;
	}

	private class JulianCalendar {
		int year;
		int month;
		int day;

		public JulianCalendar(int year, int month, int day) {
			this.year = year;
			this.month = month;
			this.day = day;
		}

		public int getYear() {
			return year;
		}

		public int getMonth() {
			return month;
		}

		public int getDay() {
			return day;
		}
	}

	// End

}
