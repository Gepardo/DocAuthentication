package ir.ut.DocAuthentication;

import java.io.Serializable;

public class InputDoc implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private byte[] docByteArray;
	private String key;
	private String expiryDate;
	private String docBase64;
	private String name;
	private String docNumber;
	private String docDate;
	
	
	public byte[] getDocByteArray() {
		return docByteArray;
	}
	public void setDocByteArray(byte[] docByteArray) {
		this.docByteArray = docByteArray;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getDocBase64() {
		return docBase64;
	}
	public void setDocBase64(String docBase64) {
		this.docBase64 = docBase64;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDocNumber() {
		return docNumber;
	}
	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}
	public String getDocDate() {
		return docDate;
	}
	public void setDocDate(String docDate) {
		this.docDate = docDate;
	}
	
	
	
	

}
