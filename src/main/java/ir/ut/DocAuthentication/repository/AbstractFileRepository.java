package ir.ut.DocAuthentication.repository;

import ir.ut.DocAuthentication.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AbstractFileRepository extends JpaRepository<AbstractFile, Long>{

}
