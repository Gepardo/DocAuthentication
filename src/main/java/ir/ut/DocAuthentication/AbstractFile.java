package ir.ut.DocAuthentication;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "abstractFiles")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"creationDate"}, allowGetters = true)

public class AbstractFile implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

	@Column(name = "displayName")
    private String displayName;
	
	@Column(name = "docDate")
    private Long docDate;
	
	@Column(name = "docNumber")
    private String docNumber;
	
	@Column(name = "key")
    private String key;

    @Column(name = "hashCode")
    private String hashCode;
    
    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date creationDate;
    
    @Column(name = "expiryDate")
	private Long expiryDate;

    protected AbstractFile(){
    }
    
    public AbstractFile(String key, String hashCode, Long expiryDate){
    	this.key = key;
    	this.hashCode = hashCode;
    	this.expiryDate = expiryDate;
    }
    
    public AbstractFile(String displayName, Long docDate, String docNumber, String key, String hashCode, Long expiryDate){
    	this.displayName = displayName;
    	this.docDate = docDate;
    	this.docNumber = docNumber;
    	
    	this.key = key;
    	this.hashCode = hashCode;
    	this.expiryDate = expiryDate;
    	
    }
    
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getHashCode() {
		return hashCode;
	}

	public void setHashCode(String hashCode) {
		this.hashCode = hashCode;
	}

	public Long getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Long expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Long getDocDate() {
		return docDate;
	}

	public void setDocDate(Long docDate) {
		this.docDate = docDate;
	}

	public String getDocNumber() {
		return docNumber;
	}

	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}
	
	
    
    
    

}
