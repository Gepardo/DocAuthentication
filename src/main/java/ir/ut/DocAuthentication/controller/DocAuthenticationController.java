package ir.ut.DocAuthentication.controller;

import static org.springframework.data.domain.ExampleMatcher.matching;
import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.ignoreCase;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Example;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import ir.ut.DocAuthentication.AbstractFile;
import ir.ut.DocAuthentication.InputDoc;
import ir.ut.DocAuthentication.exception.ResourceBadRequestException;
import ir.ut.DocAuthentication.model.DocAuthenticationMgr;
import ir.ut.DocAuthentication.repository.AbstractFileRepository;
import ir.ut.utility.Importer;

@RestController
@RequestMapping("/doc_authentication/api")
public class DocAuthenticationController {

	@Autowired
	AbstractFileRepository abstractFileRepository;

	@Value("${docStoragePath}")
	private String docStoragePath;

	@CrossOrigin
	@PostMapping("/addDoc")
	// @RequestMapping(value="/addDoc",
	// method={RequestMethod.OPTIONS,RequestMethod.POST})
	public AbstractFile createNote(@RequestBody InputDoc inputDoc) {

		byte[] docByteArray = inputDoc.getDocByteArray();

		String docDate = inputDoc.getDocDate();
		String docNumber = inputDoc.getDocNumber();
		String name = inputDoc.getName();

		if ((docDate == null || docDate.isEmpty() || docNumber == null || docNumber.isEmpty() || name == null
				|| name.isEmpty()) || (docByteArray == null || docByteArray.length == 0)) {
			throw new ResourceBadRequestException("fields are null !");
		}

		Long docDateAsLong = convertDateToLong(docDate);
		Long expiryDateAsLong = -1L;
		if (inputDoc.getExpiryDate() != null && !inputDoc.getExpiryDate().isEmpty()) {
			expiryDateAsLong = convertDateToLong(inputDoc.getExpiryDate());
		}

		String key = (inputDoc.getKey() != null) ? inputDoc.getKey() : null;
		return DocAuthenticationMgr.getInstance(abstractFileRepository).addDoc(docByteArray, docDateAsLong, docNumber,
				name, expiryDateAsLong, key, docStoragePath);
	}

	private Long convertDateToLong(String dateInString) {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date date = formatter.parse(dateInString);
			return date.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return 0L;
	}

	@CrossOrigin
	@PostMapping("/getDoc")
	public ObjectNode getNoteById(@RequestBody InputDoc inputDoc) {

		byte[] docByteArray = null;
		String rawBase64 = inputDoc.getDocBase64();
		String rawKey = inputDoc.getKey();
		if ((rawBase64 != null && !rawBase64.isEmpty()) || (rawKey != null && !rawKey.isEmpty())) {

			try {
				int index = rawBase64.indexOf(",");
				if (index == -1)
					return null;
				docByteArray = Base64.getDecoder().decode(rawBase64.substring(index + 1).getBytes("UTF-8"));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}

			if ((rawKey == null || rawKey.isEmpty()) && (docByteArray == null || docByteArray.length == 0)) {
				throw new ResourceBadRequestException("docByteArray or key is null !");
			}

			ObjectNode result = JsonNodeFactory.instance.objectNode();
			result.put("contentType", "image/gif");
			result.put("base64", inputDoc.getDocBase64().substring(inputDoc.getDocBase64().indexOf(",") + 1));
			return result;

			// TODO extract QR_Code from pic and decrypt it
			// TODO decrypt rawKey

			// TODO find pic from database!

			// return null;
			// return noteRepository.findById(noteId)
			// .orElseThrow(() -> new ResourceNotFoundException("Note", "id",
			// noteId));
		} else {
			String displayName = inputDoc.getName();
			String docDate = inputDoc.getDocDate();
			String docNumber = inputDoc.getDocNumber();
			if (displayName == null || docDate == null || docNumber == null || displayName.isEmpty()
					|| docDate.isEmpty() || docNumber.isEmpty()) {
				throw new ResourceBadRequestException("please complete fields !");
			}

			Long docDateAsLong = DocAuthenticationMgr.getInstance().convertDateToGregorianCal(docDate);

			AbstractFile abstractFile = new AbstractFile(displayName, docDateAsLong, docNumber, null, null, null);
			Example<AbstractFile> example = Example.of(abstractFile,
					matching().withIgnorePaths("key").withIgnorePaths("hashCode").withIgnorePaths("expiryDate")
							.withMatcher("displayname", ignoreCase()).withMatcher("docdate", ignoreCase())
							.withMatcher("docdate", ignoreCase()));
			Optional<AbstractFile> dd = abstractFileRepository.findOne(example);
			AbstractFile abs = dd.get();

			String hashCode = abs.getHashCode();
			String finalPath = docStoragePath;
			for (int index = 0; index < 10; index++) {
				finalPath = finalPath + File.separator + hashCode.substring(0, 2);
				hashCode = hashCode.substring(2);
			}
			finalPath = finalPath + File.separator + hashCode;

			File file = new File(finalPath);
			if (!file.exists()) {
				throw new ResourceBadRequestException("File not found !");
			}
			byte[] bytes = null;
			try {
				bytes = FileUtils.readFileToByteArray(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
			String encoded = Base64.getEncoder().encodeToString(bytes);

			ObjectNode result = JsonNodeFactory.instance.objectNode();
			result.put("contentType", "image/gif");
			result.put("base64", encoded);
			return result;
		}

	}
	
	
	@CrossOrigin
	@GetMapping("/addBatchData")
	public void addBatch() throws FileNotFoundException, IOException {
		Importer importer = new Importer();
		Importer.setAbstractFileRepository(abstractFileRepository);
		Importer.setAssetstorePath(docStoragePath);
		importer.addData();
	}
	

	@GetMapping("/getCode")
	public void getcode(@RequestBody InputDoc inputDoc) {
		//return qr-code
	}

}
