package ir.ut.utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;

import ir.ut.DocAuthentication.model.DocAuthenticationMgr;
import ir.ut.DocAuthentication.repository.AbstractFileRepository;


public class Importer {
	
	private static String DIR_PATH = "/opt/validate/importer";
	private static String DISPLAY_NAME = "displayName";
	private static String DOC_NUMBER = "docNumber";
	private static String DOC_DATE = "docDate";

	private static AbstractFileRepository abstractFileRepository;
	
	public static void setAbstractFileRepository(AbstractFileRepository abstractFileRepository) {
		Importer.abstractFileRepository = abstractFileRepository;
	}
	
	private static String assetstorePath;
	
	

	public static void setAssetstorePath(String assetstorePath) {
		Importer.assetstorePath = assetstorePath;
	}

	public void addData() throws FileNotFoundException, IOException {
		File mainFolder = new File(DIR_PATH);
		ArrayList<File> fileList = new ArrayList<File>();
		Map<String, String> keyValueMap = new HashMap<String, String>();
		for (File folder : mainFolder.listFiles()) {

			fileList.removeAll(fileList);
			fileList.clear();
			keyValueMap.clear();

			for (File file : folder.listFiles()) {
				if (file.getName().endsWith(".txt")) {
					try (BufferedReader br = new BufferedReader(new FileReader(file))) {
						for (String line; (line = br.readLine()) != null;) {
							String[] fields = line.split(",");
							if (fields != null && fields.length == 3) {
								keyValueMap.put(DISPLAY_NAME, fields[0].trim());
								keyValueMap.put(DOC_NUMBER, fields[1].trim());
								keyValueMap.put(DOC_DATE, fields[2].trim());
							}
						}
					}
				} else {
					
//					sd
					// TODO @Amin: add wetermark
					fileList.add(file);
				}
			} // end of second for

			if (!keyValueMap.isEmpty()) {
				for (File file : fileList) {
					DocAuthenticationMgr.getInstance(abstractFileRepository).addDoc(FileUtils.readFileToByteArray(file),
							DocAuthenticationMgr.getInstance().convertDateToGregorianCal(keyValueMap.get(DOC_DATE)),
							keyValueMap.get(DOC_NUMBER), keyValueMap.get(DISPLAY_NAME), -1l, null, assetstorePath);
				}
			}

		} // end of first for

		System.out.println("Finish !");

	}

	public static void main(String[] args) {
		
		new SpringApplicationBuilder(Importer.class)
        .web(WebApplicationType.NONE) // .REACTIVE, .SERVLET
        .run(args);
		
		Importer importer = new Importer();
		try {
			importer.addData();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
