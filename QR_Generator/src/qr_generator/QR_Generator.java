

package qr_generator;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.DecodeHintType;
//import com.google.zxing.qrcode.QRCodeWriter;

import org.apache.pdfbox.pdmodel.PDDocument;
//import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.tools.imageio.ImageIOUtil;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImage;
import com.itextpdf.text.pdf.PdfIndirectObject;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

import java.awt.*;
import java.awt.geom.Rectangle2D;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.multi.qrcode.detector.MultiDetector;
import com.google.zxing.common.DetectorResult;
import com.google.zxing.common.DecoderResult;
import com.google.zxing.qrcode.decoder.Decoder;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import java.awt.geom.AffineTransform;
import javax.imageio.ImageIO;
//import javax.imageio.ImageWriter;
//import javax.imageio.stream.ImageOutputStream;
//import javax.imageio.ImageWriteParam;
//import javax.imageio.IIOImage;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.EnumMap;
//import java.util.HashMap;
//import java.util.Hashtable;
import java.util.Map;

import java.awt.Font;
import java.io.InputStream;
//import java.io.OutputStream;
//import org.apache.pdfbox.io.RandomAccessOutputStream;
//import org.apache.pdfbox.pdmodel.PDPage;

public class QR_Generator {


    private static final String QR_CODE_IMAGE_PATH = "./MyQRCode.png";
    private static final String QR_CODE_PDF_PATH = "/home/dspace/Documents/qr_pdf.pdf";
    private static final String QR_CODE_IMAGE_PATH2 = "/home/dspace/Documents/qr_image.png";
    private static final String QR_CODE_IMAGE_PATH3 = "/home/dspace/Documents/qr_watermark.png";
    private static final String EMPTY_PDF_PATH = "/home/dspace/Documents/empty_pdf.pdf";

///////////////////////////////////////////////////////////////////////////////////////////
public static String MD5CheckSum(String str) throws IOException, NoSuchAlgorithmException{
    	String password = str;
    	
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(password.getBytes());
        
        byte byteData[] = md.digest();

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
         sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
     
        System.out.println("Digest(in hex format):: " + sb.toString());

        return sb.toString();
       
    }
 ///////////////////////////////////////////////////////////////////////////////////
// generate QR Code from the String (text) and place the code in the file (filePath)
    private static void generateQRCodeImage(String text, int width, int height, String filePath)
            throws WriterException, IOException {

        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        Map<EncodeHintType, Object> hintMap = new EnumMap<>(EncodeHintType.class);
        hintMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        hintMap.put(EncodeHintType.MARGIN, 1); /* default = 4 */
        hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
        BitMatrix bitMatrix = multiFormatWriter.encode(text, BarcodeFormat.QR_CODE, width, height, hintMap);
        Path path = FileSystems.getDefault().getPath(filePath);
        
        MatrixToImageWriter.writeToPath(bitMatrix, "PNG", path);

    }
 // this code is to generate pdf file. [lace QR image (url:qr) in the blank pdf (url:src) and save in destination pdf file (url:dest). 
 // This code is only to generate a PDF file with QR code and does not need it  
    public static void manipulatePdf(String src, String dest, String qr) throws IOException, DocumentException {
        PdfReader reader = new PdfReader(src);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
        Image image = Image.getInstance(qr);
        PdfImage stream = new PdfImage(image, "", null);
        stream.put(new PdfName("ITXT_SpecialId"), new PdfName("123456789"));
        PdfIndirectObject ref = stamper.getWriter().addToBody(stream);
        image.setDirectReference(ref.getIndirectReference());
        image.setAbsolutePosition(100, 400);
        PdfContentByte over = stamper.getOverContent(1);
        over.addImage(image);
        stamper.close();
        reader.close();
 }
 // Convert pdf file with  QR code to Image to read the QR code from the image   
    public static void pdftoImage(String src) throws IOException {
       try (PDDocument document = PDDocument.load(new File(src))) {
            PDFRenderer pdfRenderer = new PDFRenderer(document);
            BufferedImage bim = pdfRenderer.renderImageWithDPI(0, 288, ImageType.RGB);
            String fileName = QR_CODE_IMAGE_PATH2;
            ImageIOUtil.writeImage(bim, fileName, 290);
            document.close();
        }
       
    }
    /////////////////////////////////////////////////////////////////////
    /*public static void savePdfAsTiff(String src, String outputUrl) throws IOException {
            PDDocument document=null;   
            document = PDDocument.load(new File(src));
            PDFRenderer pdfRenderer = new PDFRenderer(document);
            BufferedImage[] images = new BufferedImage[document.getNumberOfPages()];
            System.out.println(document.getNumberOfPages());
            for (int i = 0; i < images.length; i++) {
                PDPage page = (PDPage) document.getPage(i);
                try {
                    System.out.println(i);
                    BufferedImage image = pdfRenderer.renderImageWithDPI(i, 290, ImageType.RGB); // its throwing outofmemory error at this line
                    images[i] = image;
                    System.out.println(image.toString());
                } catch (IOException e) {
                    System.out.println("Exception while reading merged pdf file:" + e.getMessage());
                    throw e;
                }
            }
            document.close();
            File tempFile = new File(outputUrl+".tiff");
            System.out.println(ImageIO.getImageWritersByFormatName("tiff").next().toString());
            ImageWriter writer = ImageIO.getImageWritersByFormatName("tiff").next();
            ImageOutputStream output = ImageIO.createImageOutputStream(tempFile);
            writer.setOutput(output);
            ImageWriteParam params = writer.getDefaultWriteParam();
            params.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            writer.prepareWriteSequence(null);
        for (BufferedImage image : images) {
            params.setCompressionType("JPEG");
            writer.writeToSequence(new IIOImage(image, null, null), params);
        }
            writer.endWriteSequence();

    }*/
    /////////////////////////////////////////////////////////////////////////////////
   /* public void savePdfAsTiff2(PDDocument pdf, OutputStream outputStream) throws IOException {
        BufferedImage[] images = new BufferedImage[pdf.getNumberOfPages()];
        for (int i = 0; i < images.length; i++) {
            PDPage page = (PDPage) pdf.getDocumentCatalog().getPages().get(i);
            BufferedImage image;
            try {
                image = page.convertToImage(BufferedImage.TYPE_INT_RGB, 288); //works
                images[i] = image;
            } catch (IOException e) {
                e.printStackTrace();
                throw e;
            }
        }

        RandomAccessOutputStream rout = new MemoryCacheRandomAccessOutputStream(outputStream);

        ImageParam.ImageParamBuilder builder = ImageParam.getBuilder();
        ImageParam[] param = new ImageParam[1];
        TIFFOptions tiffOptions = new TIFFOptions();
        tiffOptions.setTiffCompression(TiffFieldEnum.Compression.CCITTFAX4);
        builder.imageOptions(tiffOptions);
        builder.colorType(ImageColorType.FULL_COLOR).ditherMatrix(DitherMatrix.getBayer8x8Diag()).applyDither(true).ditherMethod(DitherMethod.BAYER);
        param[0] = builder.build();

        TIFFTweaker.writeMultipageTIFF(rout, param, images);

        rout.close();
    }*/
    ////////////////////////////////////////////////////////////////////////////////
    // This code decodes the QR code in the image (File qrCodeimage) containing the QR code and reads the hash. 
    // This code detects the location of the QR code in the image
     private static String decodeQRCode(File qrCodeimage) throws IOException, NotFoundException, FormatException, ChecksumException {
       
        Map<DecodeHintType, Object> hintMap = new EnumMap<>(DecodeHintType.class);
        hintMap.put(DecodeHintType.CHARACTER_SET, "UTF-8");
        BufferedImage bufferedImage = ImageIO.read(qrCodeimage);
                    
        BufferedImageLuminanceSource source = new BufferedImageLuminanceSource(bufferedImage);
        
        HybridBinarizer hb = new HybridBinarizer(source);
        
        BitMatrix bitmatrix = hb.getBlackMatrix();   
        MultiDetector detector = new MultiDetector(bitmatrix);
        DetectorResult dResult = detector.detect(hintMap);
        BitMatrix QRImageData = dResult.getBits();
        Decoder decoderData = new Decoder ();
        DecoderResult result = decoderData.decode(QRImageData);
        return result.getText();
    }
     ////////////////////////////////////////////////////////////////
     // Add watermark (String text) to the document image (File source) and watermarked image is stored in another file (File destination). 
     private static void addTextWatermark(String text, File source, File destination) throws IOException, FontFormatException {
        BufferedImage image = ImageIO.read(source);
        Graphics2D w = (Graphics2D) image.getGraphics();
        AlphaComposite alphaChannel = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.4f);
        w.setComposite(alphaChannel);
        w.setColor(Color.GRAY);
        w.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, 
          RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        String fName = "/home/dspace/Downloads/B-Nazanin/B Nazanin.ttf";
        InputStream is =  new FileInputStream(fName);
        Font nastaligh = Font.createFont(Font.TRUETYPE_FONT, is);
        w.setFont(nastaligh.deriveFont(Font.PLAIN, 160f));
        FontMetrics fontMetrics = w.getFontMetrics();
        Rectangle2D rect = fontMetrics.getStringBounds(text, w);

        // calculate center of the image

        // add text overlay to the image
        w.translate(image.getWidth() / 2.0f, image.getHeight() / 2.0f);

      // Create a rotation transform to rotate the text based on the diagonal 
      // angle of the picture aspect ratio
      AffineTransform at2 = new AffineTransform();
      double opad = image.getHeight() / (double)image.getWidth();
      double angle = Math.toDegrees(Math.atan(opad));
      double idegrees = -1 * angle;
      double theta = (2 * Math.PI * idegrees) / 360;
      at2.rotate(theta);
      w.transform(at2);

      //Now reposition our coordinates based on size (same as we would normally 
      //do to center on straight line but based on starting at center
      float x1 = (int)rect.getWidth() / 2.0f *-1;
      float y1 = (int)rect.getHeight() / 2.0f;
      w.translate(x1, y1);

        w.drawString(text, 0.0f, 0.0f);
        //ImageIO.write(watermarked, type, destination);
        ImageIO.write(image, "png", destination);
        w.dispose();
    }

     //////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////
    public static void main(String[] args) throws NotFoundException, FormatException, ChecksumException, IOException, DocumentException, NoSuchAlgorithmException, FontFormatException {
      //savePdfAsTiff("/home/dspace/Downloads/DSDOC5x-ValidatingCheckSumsofBitstreams-240118-0048-243290.pdf","/home/dspace/Downloads/DSDOC5x-ValidatingCheckSumsofBitstreams-240118-0048-243290");
       try {
            generateQRCodeImage(MD5CheckSum("معصومه کرمعلی/۸۱۰۱۹۳۲۵۳/محمد/85763859"), 100, 100, QR_CODE_IMAGE_PATH);
        } catch (WriterException e) {
            System.out.println("Could not generate QR Code, WriterException :: " + e.getMessage());
        } catch (IOException e) {
            System.out.println("Could not generate QR Code, IOException :: " + e.getMessage());
        }
            
        manipulatePdf(EMPTY_PDF_PATH, QR_CODE_PDF_PATH,QR_CODE_IMAGE_PATH);
        
        try {
        pdftoImage(QR_CODE_PDF_PATH);
        
        }
        catch (IOException e){
            System.err.println("Exception while trying to create pdf document - " + e);
        }
        
        String text = "مرکز فناوری اطلاعات دانشگاه تهران";
        File input = new File(QR_CODE_IMAGE_PATH2);
        File output = new File(QR_CODE_IMAGE_PATH3);

        // adding text as overlay to an image
        addTextWatermark(text, input, output);

    try {
        
            File file = new File(QR_CODE_IMAGE_PATH3);
            String decodedText = decodeQRCode(file);
            if(decodedText == null) {
                System.out.println("No QR Code found in the image");
            } else {
                System.out.println("Decoded text = " + decodedText);
            }
        } catch (IOException e) {
            System.out.println("Could not decode QR Code, IOException :: " + e.getMessage());
        }
}
 
}
